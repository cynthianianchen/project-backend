package camt.se234.project.service;

import camt.se234.project.dao.UserDao;
import camt.se234.project.entity.User;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AuthenticationImplTest {

    UserDao userDao;
    AuthenticationServiceImpl authenticationService;


    @Before
    public void setUp() {
        userDao = mock(UserDao.class);
        authenticationService = new AuthenticationServiceImpl();
        authenticationService.setUserDao(userDao);
    }

    @Test
    public void testAuthenticateWithMock() {


//        List<User> mockUsers = new ArrayList<>();
//        mockUsers.add(new User(111L, "admin", "admin", "admin"));
//        mockUsers.add(new User(222L, "hello", "hello", "user"));
//        mockUsers.add(new User(333L, "user", "user", "user"));

//        when(userDao.getUser("admin", "admin")).thenReturn(new User(111L, "admin", "admin", "admin"));
//        when(userDao.getUser("hello", "hello")).thenReturn(new User(222L, "hello", "hello", "user"));
//        when(userDao.getUser("user", "user")).thenReturn(new User(333L, "user", "user", "user"));
//
//        assertThat(authenticationService.authenticate("admin", "admin"), is("admin"));

        User testUser1 = User.builder().id(111L).username("xingxing").password("xingxing").role("user").build();
        when(userDao.getUser("xingxing","xingxing")).thenReturn(testUser1);
        assertThat(authenticationService.authenticate("xingxing","xingxing"),is(testUser1));



        User testUser2 = User.builder().id(222L).username("xing").password("xing").role("user").build();
        when(userDao.getUser("xing","xing")).thenReturn(testUser2);
        assertThat(authenticationService.authenticate("123xing","123xingxing"),nullValue());





    }
}