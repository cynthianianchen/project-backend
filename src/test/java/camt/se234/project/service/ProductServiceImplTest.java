package camt.se234.project.service;

import camt.se234.project.dao.ProductDao;
import camt.se234.project.entity.Product;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.is;

public class ProductServiceImplTest {

    ProductDao productDao;
    ProductServiceImpl productService;

    @Before
    public void setUp(){
        productDao = mock (ProductDao.class);
        productService = new ProductServiceImpl();
        productService.setProductDao(productDao);

}

    @Test
    public void testGetAllProductsWithMock(){
        List<Product> mockProducts = new ArrayList<>();
        mockProducts.add(new Product(11l,"p0001","Garden","The garden which you can grow everything on earth","garden.jpg",20000));
        mockProducts.add(new Product(22l,"p0002","Banana","A good fruit with very cheap price","banana.jpg",150));
        mockProducts.add(new Product(33l,"p0003","Orange","Nothing good about it","orange.jpg",280));
        mockProducts.add(new Product(44l,"p0004","Papaya","Use for papaya salad","papaya.jpg",12));
        mockProducts.add(new Product(55l,"p0005","Rambutan","An expensive fruit from the sout","rambutan.jpg",20));
        mockProducts.add(new Product(66l,"p0006","Unknown","Don't use this","rambutan.jpg",-1));

        when(productService.getAllProducts()).thenReturn(mockProducts);
        assertThat(productService.getAllProducts(),hasItems(new Product(11l,"p0001","Garden","The garden which you can grow everything on earth","garden.jpg",20000),
                new Product(22l,"p0002","Banana","A good fruit with very cheap price","banana.jpg",150),
                new Product(33l,"p0003","Orange","Nothing good about it","orange.jpg",280),
                new Product(44l,"p0004","Papaya","Use for papaya salad","papaya.jpg",12),
                new Product(55l,"p0005","Rambutan","An expensive fruit from the sout","rambutan.jpg",20),
                new Product(66l,"p0006","Unknown","Don't use this","rambutan.jpg",-1)
                ));


    }


    @Test
    public void testGetAvailableProductsWithMock(){
        List<Product> mockAvailableProducts = new ArrayList<>();
        mockAvailableProducts.add(new Product(11l,"p0001","Garden","The garden which you can grow everything on earth","garden.jpg",20000));
        mockAvailableProducts.add(new Product(22l,"p0002","Banana","A good fruit with very cheap price","banana.jpg",150));
        mockAvailableProducts.add(new Product(33l,"p0003","Orange","Nothing good about it","orange.jpg",280));
        mockAvailableProducts.add(new Product(44l,"p0004","Papaya","Use for papaya salad","papaya.jpg",12));
        mockAvailableProducts.add(new Product(55l,"p0005","Rambutan","An expensive fruit from the sout","rambutan.jpg",20));
        mockAvailableProducts.add(new Product(66l,"p0006","Unknown","Don't use this","rambutan.jpg",-1));

        when(productService.getAvailableProducts()).thenReturn(mockAvailableProducts);
        assertThat(productService.getAvailableProducts(),hasItems(new Product(11l,"p0001","Garden","The garden which you can grow everything on earth","garden.jpg",20000),
                new Product(22l,"p0002","Banana","A good fruit with very cheap price","banana.jpg",150),
                new Product(33l,"p0003","Orange","Nothing good about it","orange.jpg",280),
                new Product(44l,"p0004","Papaya","Use for papaya salad","papaya.jpg",12),
                new Product(55l,"p0005","Rambutan","An expensive fruit from the sout","rambutan.jpg",20)));


    }

    @Test
    public void testGetUnavailableProductSizeWithMock(){
        List<Product> mockUnavailableProducts = new ArrayList<>();
        mockUnavailableProducts.add(new Product(66l,"p0006","Unknown","Don't use this","rambutan.jpg",-1));
        when(productService.getAllProducts()).thenReturn(mockUnavailableProducts);
        assertThat(productService.getUnavailableProductSize(),is(1));





    }


}
