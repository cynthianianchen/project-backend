package camt.se234.project.service;

import camt.se234.project.dao.OrderDao;
import camt.se234.project.entity.Product;
import camt.se234.project.entity.SaleOrder;
import camt.se234.project.entity.SaleTransaction;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;

import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.*;

public class SaleOrderServiceImplTest {

    OrderDao orderDao;
    SaleOrderServiceImpl saleOrderService;

    static List<SaleOrder> mockSaleOrders;
    @Before
    public void setUp(){
        orderDao = mock (OrderDao.class);
        saleOrderService = new SaleOrderServiceImpl();
        saleOrderService.setOrderDao(orderDao);

        when(orderDao.getOrders()).thenReturn(mockSaleOrders);
    }
    @BeforeClass
    public static void initialData(){
        List<Product> mockProducts = new ArrayList<>();
        mockProducts.add(Product.builder().id(11l).productId("p0001").name("Garden").description("The garden which you can grow everything on earth").price(20000).build());
        mockProducts.add(Product.builder().id(22l).productId("p0002").name("Banana").description("A good fruit with very cheap price").price(150).build());
        mockProducts.add(Product.builder().id(33l).productId("p0003").name("Orange").description("Nothing good about it").price(280).build());
        mockProducts.add(Product.builder().id(44l).productId("p0004").name("Papaya").description("Use for papaya salad").price(12).build());
        // mockProducts.add(new Product(55l,"p0005","Rambutan","An expensive fruit from the sout","rambutan.jpg",20));


        List<SaleTransaction> mockSaleTransaction = new ArrayList<>();
        mockSaleTransaction.add(SaleTransaction.builder().id(11l).transactionId("111").product(mockProducts.get(0)).amount(5).build());
        mockSaleTransaction.add(SaleTransaction.builder().id(22l).transactionId("123").product(mockProducts.get(1)).amount(10).build());
        mockSaleTransaction.add(SaleTransaction.builder().id(33l).transactionId("124").product(mockProducts.get(2)).amount(50).build());
        mockSaleTransaction.add(SaleTransaction.builder().id(44l).transactionId("125").product(mockProducts.get(3)).amount(1).build());




        mockSaleOrders = new ArrayList<>();
        mockSaleOrders.add(SaleOrder.builder().id(123l).saleOrderId("111").transactions(new ArrayList<SaleTransaction>() {{ add(mockSaleTransaction.get(0));add(mockSaleTransaction.get(3));}}).build());
        mockSaleOrders.add(SaleOrder.builder().id(124l).saleOrderId("222").transactions(new ArrayList<SaleTransaction>() {{ add(mockSaleTransaction.get(2));add(mockSaleTransaction.get(1));}}).build());


    }

    @Test
    public void testGetSaleOrdersWithMock(){

        assertThat(saleOrderService.getSaleOrders(),hasItems(mockSaleOrders.get(0),mockSaleOrders.get(1)));

    }

    @Test
    public void testGetAverageSaleOrderPriceWithMock(){
        assertThat(saleOrderService.getAverageSaleOrderPrice(),is(57756.0));

    }

}
